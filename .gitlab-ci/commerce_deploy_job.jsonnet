// jsonnet -S --ext-str stage_suffix="" --ext-str stage=stg02 commerce_deploy_json.jsonnet
local clientString = std.extVar('client_list');
local stageSuffix = std.extVar('stage_suffix');
local clientList = std.split(clientString, " ");

local include = {
  include: [{
    project: "fabric2-public/cicd/cicd-templates",
    ref: "main",
    file: ["templates/sls.yml"]
  }]
};

local workflow = {
  workflow: {
    rules: [
      {
        "if": "$CI_COMMIT_BRANCH == \"main\""
      },
    ],
  },
};

local stages = {
  stages: [
    "deploy-commerce"
  ]
};

local commerce_deploy_job(clientStage) =
  local clientAndStage = std.split(clientStage, ":");
  local client = clientAndStage[0];
  local stage = clientAndStage[1];
  {

    stage: 'deploy-commerce',
    extends: '.sls-deploy-14',
    environment: {
      name: '$SANDBOX_ENV_NAME/' + client + '/' + stage + stageSuffix
    },
    variables: {
      STAGE: stage + stageSuffix,
      CLIENT: client,
      PLATFORM: 'commerce',
    },
    when: 'manual',
    needs: []
  };

local jobs = {[clientStage]: 
  commerce_deploy_job(clientStage) for clientStage in clientList };

local echoJob = {
  echoJob: {
    stage: 'deploy-commerce',
    image: 'alpine',
    script: [
      'echo "This job is needed to keep the pipeline from failing. Child pipelines need at least one non-manual job."',
      'env'
    ],
    needs: []
  },
};

local gitlabCiConf = include + workflow + stages + jobs + echoJob;

std.manifestYamlDoc(gitlabCiConf)