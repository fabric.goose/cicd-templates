# Postman Template

Runs a postman collection.

## Variables

| Variable Name | Description | Required | Default |
| ---- | ---- | ---- | ---- |
| POSTMAN_API_KEY | Postman API key. Get it [here](https://fabricteam.postman.co/settings/me/api-keys) | yes |
| POSTMAN_ENVIRONMENT_UUID | The environment UUID; can be obtained from the [Postman API](https://community.postman.com/t/where-do-you-see-your-own-uid-for-collection/3537) | yes | |
| POSTMAN_COLLECTION_UUID | The collection UUID; can be obtained from the [Postman API](https://community.postman.com/t/where-do-you-see-your-own-uid-for-collection/3537) | yes | |

## Usage

```yaml
.postman:
  stage: api-testing
  variables:
    POSTMAN_API_KEY: <key>
    POSTMAN_COLLECTION_UUID: <uuid>
    POSTMAN_ENVIRONMENT_UUID: <uuid>
```

## FAQ

### Where do I generate an API key?

https://fabricteam.postman.co/settings/me/api-keys

### Where do I find the environment and collection UUIDs?

The easiest way is to query the workspace using the workspace UID. You can find it in the URL when in the workspace on the browser.

```sh
curl -L -H "X-API-KEY:$API_KEY" https://api.getpostman.com/workspaces/{workspace_uid}
```
