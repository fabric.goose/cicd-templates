# See https://yottadv.atlassian.net/wiki/spaces/DEV/pages/2074083587/Gitlab+Serverless+Pipeline 
# for documentation for this pipeline
include:
  - project: fabric2-public/cicd/cicd-templates
    ref: main
    file:
      - templates/sls.yml
      - templates/postman.yml
      - templates/code_quality.yml
      - templates/a11y.yml
      - templates/npm.yml
      - templates/sast.yml
      - templates/secret_detection.yml
      - templates/browser_performance.yml
      - templates/platform.yml

variables:
  # BUILD_ID can be modified from $CI_COMMIT_SHORT_SHA to a previous commit SHA, in which case
  # the pipeline will fetch the previous build from the registry rather than rebuild. 
  # Useful mostly in debugging situations for quick feedback loops.
  BUILD_ID: $CI_COMMIT_SHORT_SHA
  DEV_ENV_NAME: DEV
  QA_ENV_NAME: QA
  INT_UAT_ENV_NAME: INT-UAT
  SANDBOX_ENV_NAME: SANDBOX
  PROD_ENV_NAME: PROD

  DEV_COMMERCE_STAGE: dev01
  DEV_COPILOT_STAGE: dev02
  DEV_COMMERCE_CLIENT: bailey
  DEV_COPILOT_CLIENT: greatwall
  QA_STAGE: stg02
  QA_COMMERCE_CLIENT: bailey
  QA_COPILOT_CLIENT: greatwall
  INT_UAT_STAGE: prod02
  INT_UAT_COMMERCE_CLIENT: bailey-prod02
  INT_UAT_COPILOT_CLIENT: greatwall-production
  SANDBOX_STAGE: sandbox
  SANDBOX_COPILOT_CLIENT: greatwall-sandbox
  PROD_STAGE: prod01
  PROD_COPILOT_CLIENT: greatwall-production

  # Serverless variables
  WARMUP_ENABLE: "true"
  SLS_DEBUG: "*"


  # SKIP_LINT: ''
  # SKIP_UNIT_TEST: ''
  # SKIP_SONARCLOUD: ''
  # SKIP_CODE_QUALITY: ''
  # SKIP_SEMGREP: ''
  # SKIP_SECRET_SCANNER: ''
  # SKIP_QA_COMMERCE: ''
  # SKIP_QA_COPILOT: ''
  
  # DEV_COPILOT_STAGE: ''
  # DEV_COMMERCE_STAGE: ''
  # QA_STAGE: ''
  # INT_UAT_STAGE: ''
  # SANDBOX_STAGE: ''
  # PROD_STAGE: ''


stages:
- dependencies
- validate
- build
- test
- deploy-DEV
- test-DEV
- deploy-QA
- test-QA
- deploy-INT-UAT
- test-INT-UAT
- prepare-SANDBOX
- deploy-SANDBOX
- prepare-PROD
- deploy-PROD

.rules-anchor:
  rules:
    - &merge-request 
      if: $CI_MERGE_REQUEST_ID 
    - &main-or-merge-request 
      if: '$CI_COMMIT_BRANCH == "main" || $CI_MERGE_REQUEST_ID'
    - &main-branch
      if: '$CI_COMMIT_BRANCH == "main"'
      when: manual
    - &env-dump 
      if: $ENABLE_ENV_DUMP != "true"
      when: never
    - &skip-lint
      if: $SKIP_LINT
      when: never
    - &skip-tests 
      if: $SKIP_UNIT_TEST
      when: never
    - &skip-codequality 
      if: $SKIP_CODE_QUALITY
      when: never
    - &skip-sonarcloud 
      if: $SKIP_SONARCLOUD
      when: never
    - &skip-semgrep 
      if: $SKIP_SEMGREP
      when: never
    - &skip-secretscanner 
      if: $SKIP_SECRET_SCANNER
      when: never
    - &skip-dev-copilot-stage 
      if: $DEV_COPILOT_STAGE == null || $DEV_COPILOT_STAGE == ""
      when: never
    - &skip-qa-copilot-stage
      if: $SKIP_QA_COPILOT
      when: never
    - &skip-dev-commerce-stage
      if:  $DEV_COMMERCE_STAGE == null || $DEV_COMMERCE_STAGE == ""
      when: never
    - &skip-qa-commerce-stage
      if: $SKIP_QA_COMMERCE
      when: never
    - &skip-postman 
      if: $POSTMAN_API_KEY == null || $POSTMAN_API_KEY == "" || $POSTMAN_COLLECTION_UUID == null || $POSTMAN_COLLECTION_UUID == ""
      when: never
    - &skip-dev-postman 
      if: $POSTMAN_DEV02_ENVIRONMENT_UUID == null || $POSTMAN_DEV02_ENVIRONMENT_UUID == ""
      when: never
    - &skip-a11y 
      if: $A11Y_URL == null || $A11Y_URL == ""
      when: never
    - &skip-qa-stage 
      if: $QA_STAGE == null || $QA_STAGE == "" 
      when: never
    - &skip-qa-postman 
      if: $POSTMAN_STG02_ENVIRONMENT_UUID == null || $POSTMAN_STG02_ENVIRONMENT_UUID == ""
      when: never
    - &skip-intuat-postman 
      if: $POSTMAN_PROD02_ENVIRONMENT_UUID == null || $POSTMAN_PROD02_ENVIRONMENT_UUID == ""
      when: never
    - &skip-intuat-stage 
      if: $INT_UAT_STAGE == null || $INT_UAT_STAGE == ""
      when: never
    - &skip-sandbox-postman 
      if: $POSTMAN_SANDBOX_ENVIRONMENT_UUID == null || $POSTMAN_SANDBOX_ENVIRONMENT_UUID == ""
      when: never
    - &skip-sandbox-stage 
      if: $SANDBOX_STAGE == null || $SANDBOX_STAGE == ""
      when: never
    - &skip-perf-test 
      if: $BROWSER_PERF_URL == null || $BROWSER_PERF_URL == "" 
      when: never
    - &skip-prod-stage 
      if: $PROD_STAGE == null || $PROD_STAGE == ""
      when: never

workflow:
  rules:
    - *main-or-merge-request

envdump:
  image: alpine
  stage: .pre
  script: env
  rules:
    - *env-dump
    - *main-or-merge-request

lint:
  stage: validate
  extends:
    - .npm-run-10
  variables:
    npm_cmd: $NPM_RUN_LINT_CMD
  needs: []
  rules:
    - *skip-lint
    - *main-or-merge-request
  allow_failure: true

unit-test:
  stage: validate
  extends:
    - .npm-run-10
  variables:
    npm_cmd: $NPM_RUN_UNIT_TEST_CMD
  needs: []
  rules:
    - *skip-tests
    - *main-or-merge-request
  allow_failure: true

build:
  stage: build
  extends: 
    - .sls-webpack-10
  variables:
    STAGE: build
    SLS_DEBUG: '*'
  dependencies: []
  needs:
    - job: lint
      optional: true
    - job: unit-test
      optional: true
  rules:
    - *main-or-merge-request

.base-test:
  stage: test
  needs: [build]
  allow_failure: true
  dependencies: []

sonarcloud:
  image:
    name: sonarsource/sonar-scanner-cli:latest
    entrypoint: [""]
  extends:
    - .base-test
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"
    GIT_DEPTH: "0" 
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
    - sonar-scanner
  rules:
    - *skip-sonarcloud
    - *main-or-merge-request

code-quality:
  extends:
    - .code-quality
    - .base-test
  rules:
    - *skip-codequality
    - *main-or-merge-request

secret-scanner:
  extends:
    - .secret_detection_default_branch
    - .base-test
  rules:
    - *skip-secretscanner
    - *main-or-merge-request

semgrep-sast:
  cache: {}
  extends:
    - .semgrep-sast
    - .base-test
  rules:
    - *skip-semgrep
    - *main-or-merge-request

.base-deploy-DEV:
  stage: deploy-DEV
  extends:
    - .sls-deploy-10
  dependencies: []
  needs:
    - job: build
      optional: false
    - job: code-quality
      optional: true
    - job: secret-scanner
      optional: true
    - job: semgrep-sast
      optional: true
    - job: sonarcloud
      optional: true

# follows promotion path laid out here 
# https://docs.google.com/spreadsheets/d/1r9PPD_V3VyOymucosdmHgmRy8GguQhBjZWmZhKMMKJg
copilot-dev02:
  extends:
    - .base-deploy-DEV
  environment:
    name: $DEV_ENV_NAME/${DEV_COPILOT_STAGE}-copilot
  variables:
    PLATFORM: copilot
    CLIENT: $DEV_COPILOT_CLIENT
    STAGE: $DEV_COPILOT_STAGE
    PREVIOUS_STAGE: build
  rules:
    - *skip-dev-copilot-stage
    - *main-or-merge-request

commerce-dev01:
  extends:
    - .base-deploy-DEV
  environment:
    name: $DEV_ENV_NAME/${DEV_COMMERCE_STAGE}-commerce
  variables:
    PLATFORM: commerce
    CLIENT: $DEV_COMMERCE_CLIENT
    STAGE: $DEV_COMMERCE_STAGE
    PREVIOUS_STAGE: build
  rules:
    - *skip-dev-commerce-stage
    - *main-or-merge-request
  
postman-dev02:
  stage: test-DEV
  extends: 
    - .postman
  variables:
    POSTMAN_ENVIRONMENT_UUID: $POSTMAN_DEV02_ENVIRONMENT_UUID
  needs: 
    - commerce-dev01
    - copilot-dev02
  dependencies: []
  rules:
    - *skip-dev-copilot-stage
    - *skip-dev-commerce-stage
    - *skip-postman
    - *skip-dev-postman
    - *main-or-merge-request

a11y:
  stage: test-DEV
  extends: 
    - .a11y
  variables:
    a11y_url: $A11Y_URL
  allow_failure: true
  needs: 
    - commerce-dev01
    - copilot-dev02
  dependencies: []
  rules:
    - *skip-a11y
    - *skip-dev-copilot-stage
    - *skip-dev-commerce-stage
    - *main-or-merge-request

.base-deploy-QA:
  stage: deploy-QA
  extends:
    - .sls-deploy-10
  dependencies: []
  needs: 
    # - job: install-deps
    # TODO(erstaples): find a way to extend arrays in YAML
    - job: build
    # Since we don't know if these jobs will be included 
    # in the pipeline, we set these needs as optional. (e.g. A11Y_URL is empty; $DEV_COPILOT_STAGE is empty)
    - job: code-quality
      optional: true
    - job: secret-scanner
      optional: true
    - job: semgrep-sast
      optional: true
    - job: sonarcloud
      optional: true
    - job: a11y
      optional: true
    - job: postman-dev02
      optional: true
    - job: commerce-dev01
      optional: true
    - job: copilot-dev02
      optional: true
  rules:
    - *skip-qa-stage

copilot-stg02:
  extends:
    - .base-deploy-QA
  environment:
    name: $QA_ENV_NAME/${QA_STAGE}-copilot
  variables:
    PLATFORM: copilot
    CLIENT: $QA_COPILOT_CLIENT
    STAGE: $QA_STAGE
    PREVIOUS_STAGE: $DEV_COPILOT_STAGE
  rules:
    - *skip-qa-copilot-stage
    - *main-or-merge-request

commerce-stg02:
  extends:
    - .base-deploy-QA
  environment:
    name: $QA_ENV_NAME/${QA_STAGE}-commerce
  variables:
    PLATFORM: commerce
    CLIENT: $QA_COMMERCE_CLIENT
    STAGE: $QA_STAGE
    PREVIOUS_STAGE: $DEV_COMMERCE_STAGE
  rules:
    - *skip-qa-commerce-stage
    - *main-or-merge-request

postman-stg02:
  extends: 
    - .postman
  stage: test-QA
  variables:
    POSTMAN_ENVIRONMENT_UUID: $POSTMAN_STG02_ENVIRONMENT_UUID
  needs: 
    # For tests, all we need is the deploy jobs that are being tested.
    - commerce-stg02
    - copilot-stg02
  dependencies: []
  rules:
    - *skip-postman
    - *skip-qa-stage
    - *skip-qa-postman
    - *main-or-merge-request

.base-deploy-INT-UAT:
  stage: deploy-INT-UAT
  extends:
    - .sls-deploy-10
  dependencies: []
  needs: 
    # Copy stg02s needs
    - build
    - job: code-quality
      optional: true
    - job: secret-scanner
      optional: true
    - job: semgrep-sast
      optional: true
    - job: sonarcloud
      optional: true
    - job: a11y
      optional: true
    - job: postman-dev02
      optional: true
    - job: commerce-dev01
      optional: true
    - job: copilot-dev02
      optional: true
    # Add stg02 jobs. 
    - job: postman-stg02
      optional: true
    - job: commerce-stg02
      optional: true
    - job: copilot-stg02
      optional: true
  rules:
    - *skip-intuat-stage
    - *main-branch

copilot-prod02:
  extends: .base-deploy-INT-UAT
  environment:
    name: $INT_UAT_ENV_NAME/${INT_UAT_STAGE}-copilot
  variables:
    PLATFORM: copilot
    CLIENT: $INT_UAT_COPILOT_CLIENT
    STAGE: $INT_UAT_STAGE
    PREVIOUS_STAGE: $QA_STAGE

commerce-prod02:
  extends: .base-deploy-INT-UAT
  environment:
    name: $INT_UAT_ENV_NAME/${INT_UAT_STAGE}-commerce
  variables:
    PLATFORM: commerce
    CLIENT: $INT_UAT_COMMERCE_CLIENT
    STAGE: $INT_UAT_STAGE
    PREVIOUS_STAGE: $QA_STAGE

.base-test-INT-UAT:
  stage: test-INT-UAT
  dependencies: []
  needs: 
    - build
    - job: code-quality
      optional: true
    - job: secret-scanner
      optional: true
    - job: semgrep-sast
      optional: true
    - job: sonarcloud
      optional: true
    - copilot-prod02
    - commerce-prod02

browser-performance:
  extends: 
    - .browser-performance
    - .base-test-INT-UAT
  variables:
    URL: $BROWSER_PERF_URL
  rules:
    - *skip-perf-test
    - *skip-intuat-stage
    - *main-branch

postman-prod02:
  extends:
    - .postman
    - .base-test-INT-UAT
  variables:
    POSTMAN_ENVIRONMENT_UUID: $POSTMAN_PROD02_ENVIRONMENT_UUID
  rules:
    - *skip-postman
    - *skip-intuat-stage
    - *skip-intuat-postman
    - *main-branch

get-clients-sandbox:
  stage: prepare-SANDBOX
  extends: 
    - .get-clients
  variables:
    ENV_NAME: sandbox copilot
    # Legacy commerce sandboxes are in dev01
    # Current commerce sandboxes are in uat01
    STAGES: uat01 dev01 
    PREVIOUS_STAGE: $INT_UAT_STAGE
  dependencies: []
  needs:
    # Copy stg02s needs
    - build
    - job: code-quality
      optional: true
    - job: secret-scanner
      optional: true
    - job: semgrep-sast
      optional: true
    - job: sonarcloud
      optional: true
    - job: a11y
      optional: true
    - job: postman-dev02
      optional: true
    - job: commerce-dev01
      optional: true
    - job: copilot-dev02
      optional: true
    # Add stg02 jobs. 
    - job: postman-stg02
      optional: true
    - job: commerce-stg02
      optional: true
    - job: copilot-stg02
      optional: true
    # Add prod02 jobs
    - job: commerce-prod02
      optional: true
    - job: copilot-prod02
      optional: true
    - job: postman-prod02
      optional: true
    - job: browser-performance
      optional: true
  rules:
    - *skip-sandbox-stage
    - *main-branch

.base-deploy-SANDBOX:
  needs:
    # Copy stg02s needs
    - build
    - job: code-quality
      optional: true
    - job: secret-scanner
      optional: true
    - job: semgrep-sast
      optional: true
    - job: sonarcloud
      optional: true
    - job: a11y
      optional: true
    - job: postman-dev02
      optional: true
    - job: commerce-dev01
      optional: true
    - job: copilot-dev02
      optional: true
    # Add stg02 jobs. 
    - job: postman-stg02
      optional: true
    - job: commerce-stg02
      optional: true
    - job: copilot-stg02
      optional: true
    # Add prod02 jobs
    - job: commerce-prod02
      optional: true
    - job: copilot-prod02
      optional: true
    - job: postman-prod02
      optional: true
    - job: browser-performance
      optional: true
    - get-clients-sandbox
  rules:
    - *skip-sandbox-stage
    - *main-branch

commerce-sandbox:
  stage: deploy-SANDBOX
  extends:
    - .base-deploy-SANDBOX
  needs: 
    - get-clients-sandbox
  trigger:
    include:
      - artifact: generated-config.yml
        job: get-clients-sandbox
    strategy: depend
  rules:
    - *skip-sandbox-stage
    - *main-branch

copilot-sandbox:
  stage: deploy-SANDBOX
  extends:
    - .base-deploy-SANDBOX
  #   - .sls-deploy-10
  script:
    - echo "deploying copilot sandbox"
  environment:
    name: $SANDBOX_ENV_NAME/${SANDBOX_STAGE}-copilot
  variables:
    PLATFORM: copilot
    CLIENT: $SANDBOX_COPILOT_CLIENT
    STAGE: $SANDBOX_STAGE
    PREVIOUS_STAGE: $INT_UAT_STAGE
  dependencies: []
  rules:
    - *skip-sandbox-stage
    - *main-branch

get-clients-production:
  stage: prepare-PROD
  extends: 
    - .get-clients
  variables:
    ENV_NAME: live production
    STAGES: $PROD_STAGE
    PREVIOUS_STAGE: $SANDBOX_STAGE
  rules:
    - *skip-prod-stage
    - *main-branch
  dependencies: []
  needs:
    # Copy stg02s needs
    - build
    - job: code-quality
      optional: true
    - job: secret-scanner
      optional: true
    - job: semgrep-sast
      optional: true
    - job: sonarcloud
      optional: true
    - job: a11y
      optional: true
    - job: postman-dev02
      optional: true
    - job: commerce-dev01
      optional: true
    - job: copilot-dev02
      optional: true
    # Add stg02 jobs. 
    - job: postman-stg02
      optional: true
    - job: commerce-stg02
      optional: true
    - job: copilot-stg02
      optional: true
    # Add prod02 jobs
    - job: commerce-prod02
      optional: true
    - job: copilot-prod02
      optional: true
    - job: postman-prod02
      optional: true
    - job: browser-performance
      optional: true
    # Add sandbox jobs. These are required.
    - commerce-sandbox
    - copilot-sandbox

.base-deploy-PROD:
  rules:
    - *skip-prod-stage
    - *main-branch
  needs:
    # Copy stg02s needs
    - build
    - job: code-quality
      optional: true
    - job: secret-scanner
      optional: true
    - job: semgrep-sast
      optional: true
    - job: sonarcloud
      optional: true
    - job: a11y
      optional: true
    - job: postman-dev02
      optional: true
    - job: commerce-dev01
      optional: true
    - job: copilot-dev02
      optional: true
    # Add stg02 jobs. 
    - job: postman-stg02
      optional: true
    - job: commerce-stg02
      optional: true
    - job: copilot-stg02
      optional: true
    # Add prod02 jobs
    - job: commerce-prod02
      optional: true
    - job: copilot-prod02
      optional: true
    - job: postman-prod02
      optional: true
    - job: browser-performance
      optional: true
    # Add sandbox jobs. These are required.
    - commerce-sandbox
    - copilot-sandbox
    - get-clients-production

commerce-production:
  stage: deploy-PROD
  extends:
    - .base-deploy-PROD
  trigger:
    include:
      - artifact: generated-config.yml
        job: get-clients-production
    strategy: depend
  rules:
    - *skip-prod-stage
    - *main-branch
  allow_failure: true

copilot-production:
  stage: deploy-PROD
  extends:
    - .base-deploy-PROD
  #   - .sls-deploy-10
  script:
    - echo "deploying copilot production"
  environment:
    name: $PROD_ENV_NAME/${PROD_STAGE}-copilot
  variables:
    PLATFORM: copilot
    CLIENT: $PROD_COPILOT_CLIENT
    STAGE: $PROD_STAGE
    PREVIOUS_STAGE: $SANDBOX_STAGE
  dependencies: []
  rules:
    - *skip-prod-stage
    - *main-branch