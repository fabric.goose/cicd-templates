local boilerplate = importstr "lib/boilerplate.txt";
local job_templates = {
  ".ping": {
    variables: {
      PING_TRIES: "",
      PING_SLEEP_SECONDS: "",
      PING_URL: "",
      PING_STATUS_CODE_SUCCESS: ""
    },
    image: "curlimages/curl:7.76.1",
    script: importstr "scripts/ping.sh"
  }
};
boilerplate + std.manifestYamlDoc(job_templates, indent_array_in_object=true)
