
local utils = import 'lib/utils.libsonnet';
local boilerplate = importstr "lib/boilerplate.txt";

local scripts = {
  build_artifact: importstr "scripts/artifact/build.sh",
  deploy_to_s3: importstr "scripts/artifact/gitlab-deploy-to-s3.sh"
};

local image = {
  image: "registry.gitlab.com/fabric2-public/cicd/cicd-templates/node-14-toolbox:v0.0.1"
};

local stages = {
  stages: []
};

local global_vars = {
  variables: {
    GITLAB_REGISTRY_TOKEN_HTTP_HEADER: "JOB-TOKEN",
    GITLAB_REGISTRY_TOKEN_VALUE: "$CI_JOB_TOKEN",
    GITLAB_REGISTRY_ARTIFACT_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${CI_PROJECT_NAME}",
  },
};
local job_templates = {
  ".deploy-gitlab-to-s3": {
    script: utils.prepend_utils(scripts.deploy_to_s3)
  },
  ".build-artifact": {
    script: utils.prepend_utils(scripts.build_artifact)
  },
  ".artifact-upload-to-s3": {
    variables: {
      artifact_s3_path: "",
      artifact_bucket: "",
      build_environment: "$CI_ENVIRONMENT_NAME"
    },
    script: importstr "scripts/artifact/upload-s3.sh"
  },
  ".artifact-promote": {
    variables: {
      source_env_bucket: "",
      target_env_bucket: ""
    },
    script: importstr "scripts/artifact/promote.sh"
  },
  ".artifact-deploy-to-s3": {
    variables: {
      target_env_bucket: "",
      deploy_s3_bucket: "",
      build_environment: ""
    },
    script: importstr "scripts/artifact/deploy-s3.sh"
  },
  ".artifact-promote-and-deploy-to-s3": {
    variables: {
      source_env_bucket: "",
      target_env_bucket: ""
    },
    script: 
    (importstr "scripts/artifact/promote.sh") + 
    (importstr "scripts/artifact/deploy-s3.sh")
  }
};

local gitlabCiConf =  image +
                      stages +
                      global_vars +
                      job_templates;
boilerplate + std.manifestYamlDoc(gitlabCiConf, indent_array_in_object=true)
