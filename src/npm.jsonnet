local boilerplate = importstr "lib/boilerplate.txt";
local variables = {
  variables: {
    npm_dir: ".",
    BUILD_DIR: "."
  },
};
local job_templates = {
  ".node-14": {
    image: "registry.gitlab.com/fabric2-public/cicd/cicd-templates/node-14-toolbox:v0.0.1"
  },
  ".node-10": {
    image: "registry.gitlab.com/fabric2-public/cicd/cicd-templates/node:10"
  },
  ".npm-install": {
    extends: ".node-10",
    script: |||
      set -x
      if [ ! -z $BUILD_DIR ]; then cd $BUILD_DIR; fi
      if [ ! -d node_modules ]; then 
        echo //registry.npmjs.org/:_authToken=${NPM_TOKEN} > ~/.npmrc
        npm install
      fi
    |||,
    artifacts: {
      paths: [
        "**/node_modules/**",
      ],
      expire_in: "1 week"
    },
  },
  // JUnit XML result
  // Cobertura XML coverage
  ".npm-run": {
    variables: {
      NPM_JUNIT_REPORT_PATH: "./junit.xml",
      NPM_COBERTURA_REPORT_PATH: "./cobertura-coverage.xml",
    },
    script: |||
      set -x
      if [ ! -z "$BUILD_DIR" ]; then 
        cd $BUILD_DIR
      elif [ ! -z "$npm_dir" ]; then
        cd $npm_dir
      fi
      if [ ! -d node_modules ]; then 
        echo //registry.npmjs.org/:_authToken=${NPM_TOKEN} > ~/.npmrc
        npm install
      fi
      npm run "$npm_cmd"
    |||,
    artifacts: {
      paths: [
        "$NPM_JUNIT_REPORT_PATH",
        "$NPM_COBERTURA_REPORT_PATH",
      ],
      reports: {
        junit: "$NPM_JUNIT_REPORT_PATH",
        cobertura: "$NPM_COBERTURA_REPORT_PATH"
      },
    },
  },
  ".npm-run-10": {
    extends: [
      ".npm-run",
      ".node-10"
    ]
  },
  ".npm-run-14": {
    extends: [
      ".npm-run",
      ".node-14"
    ],
  },
  ".npm-install-10": {
    image: "registry.gitlab.com/fabric2-public/cicd/cicd-templates/node-10-toolbox:v0.0.1",
    extends: [
      ".npm-install"
    ],
  },
  ".npm-install-14": {
    image: "registry.gitlab.com/fabric2-public/cicd/cicd-templates/node-14-toolbox:v0.0.1",
    extends: [
      ".npm-install"
    ],
  },
};
local config = variables + job_templates;
boilerplate + std.manifestYamlDoc(config, indent_array_in_object=true)
