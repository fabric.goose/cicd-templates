local boilerplate = importstr "lib/boilerplate.txt";
local variables = {
  variables: {
    tf_dir: "",
    tf_workspace_name: "$CI_ENVIRONMENT_NAME",
    tf_vars_file_name: "$CI_ENVIRONMENT_NAME",
    PLAN: "plan.tfplan",
    PLAN_JSON: "plan.json"
  },
};
local job_templates = {
  ".base-terraform": {
    image: {
      name: "registry.gitlab.com/rafidsaad/images/terraform-hydrated",
      entrypoint: [""]
    },
    before_script: ["cd $tf_dir"]
  },
  ".terraform-init": {
    extends: ".base-terraform",
    stage: "terraform-init",
    script: importstr "scripts/terraform/init.sh",
    artifacts: {
      paths: [
        "**/.terraform",
        "**/.terraform.**"
      ],
    },
  },
  ".terraform-validate": {
    extends: ".base-terraform",
    stage: "terraform-validate",
    script: importstr "scripts/terraform/validate.sh",
  },
  ".terraform-plan": {
    extends: ".base-terraform",
    stage: "terraform-plan",
    before_script: [
      "cd $tf_dir",
      "apk --no-cache add jq",
      "shopt -s expand_aliases",
      |||
        alias convert_report="jq -r '([.resource_changes[]?.change.actions?]|flatten)|{\"create\":(map(select(.==\"create\"))|length),\"update\":(map(select(.==\"update\"))|length),\"delete\":(map(select(.==\"delete\"))|length)}'"
      |||
    ],
    script: |||
      tf_workspace_name=${tf_workspace_name:-$CI_ENVIRONMENT_NAME}
      tf_vars_file_name=${tf_vars_file_name:-$CI_ENVIRONMENT_NAME}
      terraform workspace select ${tf_workspace_name} || terraform workspace new ${tf_workspace_name}
      terraform plan -out=$PLAN -var-file=env/${tf_vars_file_name}.tfvars
      terraform show --json $PLAN | convert_report > $PLAN_JSON
    |||,
    artifacts: {
      paths: [
        "**/.terraform",
        "**/.terraform.**",
        "**/*.tfplan",
        "$tf_dir/$PLAN_JSON"
      ],
      reports: {
        terraform: "$tf_dir/$PLAN_JSON"
      },
      expire_in: "1 week"
    }
  },
  ".terraform-apply": {
    extends: ".base-terraform",
    stage: "terraform-apply",
    script: "terraform apply -auto-approve $PLAN",
    artifacts: {
      paths: [
        "$tf_dir/outputs/**/*"
      ],
    },
  },
  ".terraform-destroy": {
    extends: ".base-terraform",
    stage: "terraform-destroy",
    script: |||
      tf_workspace_name=${tf_workspace_name:-$CI_ENVIRONMENT_NAME}
      tf_vars_file_name=${tf_vars_file_name:-$CI_ENVIRONMENT_NAME}
      terraform destroy -auto-approve -var-file=env/${tf_vars_file_name}.tfvars
      terraform workspace select default
      terraform workspace delete -force ${tf_workspace_name}
    |||
  },
};
local gitlab_config = variables + job_templates;
boilerplate + std.manifestYamlDoc(gitlab_config, indent_array_in_object=true)
