local boilerplate = importstr "lib/boilerplate.txt";
local job_templates = {
  ".postman": {
    image: {
      name: "postman/newman_alpine33",
      entrypoint: [""],
    },
    cache: {},
    variables: {
      POSTMAN_COLLECTION_UUID: "",
      POSTMAN_ENVIRONMENT_UUID: ""
    },
    script: importstr "scripts/postman.sh",
    dependencies: [],
    allow_failure: true,
    artifacts: {
      reports: {
        junit: "report.xml"
      },
    },
  }
};

boilerplate + std.manifestYamlDoc(job_templates, indent_array_in_object=true)
