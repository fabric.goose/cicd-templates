#!/usr/bin/env bash

bin_dir=$(pwd)/bin
mkdir $bin_dir
curl -o $bin_dir/aws-iam-authenticator https://amazon-eks.s3.us-west-2.amazonaws.com/1.19.6/2021-01-05/bin/linux/amd64/aws-iam-authenticator
chmod +x $bin_dir/aws-iam-authenticator
export PATH=$PATH:$bin_dir

echo "Gitlab cluster create endpoint: https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/clusters/user"
api_url=$(kubectl cluster-info | grep -E 'Kubernetes master|Kubernetes control plane' | awk '/http/ {print $NF}')
token_secret_name=$(kubectl get secret -n kube-system | grep default-token | awk '{print $1}')
ca_crt=$(kubectl get secret -n kube-system $token_secret_name -o jsonpath="{['data']['ca\.crt']}" | base64 --decode)

cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: ServiceAccount
metadata:
  name: gitlab
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: gitlab-admin
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
  - kind: ServiceAccount
    name: gitlab
    namespace: kube-system
EOF
secret_name=$(kubectl -n kube-system get secret | grep gitlab | awk '{print $1}')
sa_token=$(kubectl -n kube-system get secret $secret_name -o jsonpath="{['data']['token']}")
curl --header "Job-Token:$CI_JOB_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/clusters/user" \
	-H "Accept: application/json" \
	-H "Content-Type:application/json" \
	-X POST \
	--data '{"id":"'$CI_PROJECT_ID'", "name":"'$RUNNER_NAME'", "platform_kubernetes_attributes":{"api_url":"'$api_url'","token":"'$sa_token'","namespace":"'$KUBE_NAMESPACE'","ca_cert":"'$ca_crt'"}}'
