#!/usr/bin/env bash

set -eux

echo "===== Build image => ${IMAGE} ====="
echo "===== Build options => ${DOCKER_BUILD_OPTIONS-} ====="
docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
docker pull $IMAGE || true
docker build ${DOCKER_BUILD_OPTIONS-} --cache-from $IMAGE -t $IMAGE .
docker push $IMAGE
