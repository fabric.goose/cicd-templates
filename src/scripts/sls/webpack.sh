#!/usr/bin/env bash

set -eux

build_path=${BUILD_PATH:-"${BUILD_ID}.zip"}
package_name="${PACKAGE_NAME:-$CI_PROJECT_NAME}"

cd "${BUILD_DIR}"

GITLAB_REGISTRY_ARTIFACT_URL="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${package_name}"
echo "GITLAB_REGISTRY_ARTIFACT_URL=$GITLAB_REGISTRY_ARTIFACT_URL"
http_code=$(curl -s -LI -H "JOB-TOKEN:${CI_JOB_TOKEN}" -o/dev/null -w "%{http_code}" "${GITLAB_REGISTRY_ARTIFACT_URL}"/"${STAGE}"/"${build_path}")
if [ "${http_code}" -eq 200 ]; then
	echo "artifact already exists; skipping build"
	exit 0
fi
echo //registry.npmjs.org/:_authToken="${NPM_TOKEN}" >~/.npmrc
if [ ! -d node_modules ]; then npm install; fi
sls package --package build --verbose
rm build/*.json
tar -czf build.tar.gz build serverless.yml
zip -r "${build_path}" build.tar.gz
response_code=$(curl \
	-H "JOB-TOKEN:${CI_JOB_TOKEN}" \
	-X PUT \
	--silent \
	--data-binary "@${build_path}" \
	--write-out "%{http_code}" \
	--output /dev/null "${GITLAB_REGISTRY_ARTIFACT_URL}"/"${STAGE}"/"${build_path}")
if [ "${response_code}" -ne 201 ]; then
	echo "Error response code ${response_code}"
	exit 1
fi
