#!/usr/bin/env bash
set -eux

if [[ -z ${PLATFORM_DISABLE_HISTORY} ]]; then
	apiName=${API_NAME:-"empty"}
	functionSet=${FUNCTION_SET:-"empty"}
	cat <<EOF >post.json
{
  "repo": "${CI_PROJECT_NAME}",
  "client": "${CLIENT}",
  "build": {
    "buildName": "${CLIENT}-${STAGE}-${apiName}",
    "commitTime": "${CI_COMMIT_TIMESTAMP}",
    "buildNumber": "${CI_JOB_ID}",
    "deployedBy": "${GITLAB_USER_EMAIL}",
    "commitId": "${CI_COMMIT_SHORT_SHA}",
    "functionSet": "${functionSet}",
    "platform": "${PLATFORM}",
    "apiName": "${apiName}",
    "stage": "${STAGE}",
    "jobUrl": "${CI_JOB_URL}",
    "buildStatus": "RUNNING",
    "createdAt": $(date +%s%N | cut -b1-13)
    }
  }
EOF
	curl \
		--silent \
		--location \
		--request POST \
		--header "x-api-key:${PLATFORM_API_KEY}" \
		--header "Content-Type:application/json" \
		--data-binary @post.json "${PLATFORM_BASE_URL}"/addToHistory
fi
