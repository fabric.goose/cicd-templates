#!/usr/bin/env bash

terraform fmt \
	-recursive \
	-diff \
	-write=false \
	-check \
	../
terraform validate
