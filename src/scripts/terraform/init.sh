#!/usr/bin/env bash

if [[ $# -eq 0 ]]; then
	terraform init -input=false
else
	# shellcheck disable=SC2068
	terraform init $@
fi
