#!/usr/bin/env bash

set -eux

CI_ENVIRONMENT_URL=${CI_ENVIRONMENT_URL:-""}

tries=${PING_TRIES:-5}
sleep=${PING_SLEEP_SECONDS:-5}
url=${PING_URL:-$CI_ENVIRONMENT_URL}
success_status=${PING_STATUS_CODE_SUCCESS:-200}

do_ping() {
	tries=$((tries - 1))
	response=$(curl -s -o /dev/null -w "%{http_code}" $url)
	if [ $response -eq $success_status ]; then
		echo 'Ping success'
		exit 0
	fi
}

do_ping

while [ $tries -gt 0 ]; do
	sleep $sleep
	do_ping
done
echo 'Ping failed'
exit 1
