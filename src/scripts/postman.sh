#!/usr/bin/env bash

set -eux

collection_url="https://api.getpostman.com/collections/$POSTMAN_COLLECTION_UUID?apikey=$POSTMAN_API_KEY"
environment_url="https://api.getpostman.com/environments/$POSTMAN_ENVIRONMENT_UUID?apikey=$POSTMAN_API_KEY"
newman run ${collection_url} -e ${environment_url} --reporters cli,junit --reporter-junit-export report.xml
