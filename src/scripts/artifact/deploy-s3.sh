#!/usr/bin/env bash

set -eux

if [[ -z ${target_env_bucket-} ]]; then
	echo "ERROR: missing required target_env_bucket"
fi
if [[ -z ${deploy_s3_bucket-} ]]; then
	if [[ -z ${CI_ENVIRONMENT_URL-} ]]; then
		echo "ERROR: missing deploy_s3_bucket. No CI_ENVIRONMENT_URL to default to"
		exit 1
	fi
	echo "WARNING: missing deploy_s3_bucket; defaulting to CI_ENVIRONMENT_URL: $CI_ENVIRONMENT_URL"
	deploy_s3_bucket=$(echo "$CI_ENVIRONMENT_URL" | sed 's/https:\/\///')
fi
if [[ -z ${build_environment-} ]]; then
	if [[ -z ${CI_ENVIRONMENT_NAME-} ]]; then
		echo "ERROR: missing build_environment. No CI_ENVIRONMENT_NAME to default to"
		exit 1
	fi
	echo "WARNING: missing build environment; defaulting to CI_ENVIRONMENT_NAME: $CI_ENVIRONMENT_NAME"
	build_environment=$CI_ENVIRONMENT_NAME
fi

export artifact_file=${BUILD_ID}.zip
artifact_s3_url=s3://$target_env_bucket/$CI_PROJECT_NAME/$artifact_file
echo "target artifact url $artifact_s3_url"
aws s3 cp "$artifact_s3_url" .
unzip "$artifact_file"
tar -xzvf "$build_environment".tar.gz
aws s3 sync artifact-contents "s3://$deploy_s3_bucket" --exact-timestamps --delete --acl public-read
echo "Updated ${deploy_s3_bucket} with artifact $artifact_file"
