local boilerplate = importstr "lib/boilerplate.txt";
local job_templates = {
  ".helm": {
    image: {
      name: "registry.gitlab.com/fabric2/ci-templates/kube-toolbox:latest",
    },
    variables: {
      KUBECONFIG: ""
    }
  }
};

local gitlabCiConf = job_templates;
boilerplate +  std.manifestYamlDoc(gitlabCiConf, indent_array_in_object=true)
