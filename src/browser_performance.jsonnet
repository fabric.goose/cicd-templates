local boilerplate = importstr "lib/boilerplate.txt";
local dind = import "lib/dind.libsonnet";

local stages = {
  stages: ["performance"],
};
local job_templates = {
  ".browser-performance": dind.DindJob{
    stage: "performance",
    image: "docker:git",
    variables+: {
      URL: "",
      SITESPEED_IMAGE: "sitespeedio/sitespeed.io",
      SITESPEED_VERSION: "14.1.0",
      SITESPEED_OPTIONS: ""
    },
    script: importstr "scripts/browser-performance.sh",
    artifacts: {
      paths: ["sitespeed-results/"],
      reports: {
        browser_performance: "browser-performance.json"
      },
    }
  }
};

local gitlabCiConf = stages + job_templates;
boilerplate + std.manifestYamlDoc(gitlabCiConf, indent_array_in_object=true)
