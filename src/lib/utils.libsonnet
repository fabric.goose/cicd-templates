local utils = importstr "../scripts/utils.sh";

{
  prepend_utils(script)::
    local fullScript = |||
      %(utils)s
      %(script)s
    ||| % {utils: utils, script: script};

    fullScript
}