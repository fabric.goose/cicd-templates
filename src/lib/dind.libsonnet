{
  DindJob: {
    image: "docker:19.03.12",
    services: ["docker:19.03.12-dind"],
    variables: {
      DOCKER_HOST: "tcp://docker:2375",
      # This instructs Docker not to start over TLS.
      DOCKER_DRIVER: "overlay2",
      DOCKER_TLS_CERTDIR: ""
    }
  },
}