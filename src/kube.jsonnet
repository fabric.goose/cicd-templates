local boilerplate = importstr "lib/boilerplate.txt";
local job_templates = {
  ".kube-base": {
    image: {
      name: "registry.gitlab.com/fabric2/ci-templates/kube-toolbox:latest",
    },
    variables: {
      KUBECONFIG: ""
    }
  },
  ".kube-connect-gitlab": {
    extends: ".kube-base",
    script: importstr "scripts/kubectl/connect-gitlab.sh"
  },
};

local gitlabCiConf = job_templates;
boilerplate + std.manifestYamlDoc(gitlabCiConf, indent_array_in_object=true)