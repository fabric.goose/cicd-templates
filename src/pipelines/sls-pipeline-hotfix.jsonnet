local boilerplate = importstr "../lib/boilerplate.txt";
local pipeline = {
  // This file owns the review deployment lifecycle end-to-end.
  // terraform, test, build, deploy, and teardown are bundled here.
  // Triggered by pushes to a merge request branch
  include: [
    {
      'local': 'templates/sls.yml',
    },
    {
      'local': 'templates/postman.yml',
    },
  ],
  image: 'registry.gitlab.com/fabric2/ci-templates/node:10.15.3-ee00170d',
  stages: [
    'deploy-uat',
    'test-uat',
    'deploy-prod',
    'test-prod', 
  ],
  'deploy-uat': {
    stage: 'deploy-uat',
    extends: [
      '.sls-deploy',
    ],
    environment: {
      name: 'uat',
    },
    variables: {
      CLIENT: 'greatwall',
      PLATFORM: 'copilot',
      STAGE: 'uat',
    },
    needs: [
      {
        pipeline: '$PARENT_PIPELINE_ID',
        job: 'build', 
      },
    ],
  },
  'postman-uat': {
    extends: [
      '.postman',
    ],
    stage: 'test-uat',
    variables: {
      POSTMAN_ENVIRONMENT_UUID: '$POSTMAN_UAT_ENVIRONMENT_UUID',
    },
    needs: [
      'deploy-uat',
    ],
  },
  'deploy-prod': {
    stage: 'deploy-prod',
    extends: [
      '.sls-deploy',
    ],
    cache: {
    },
    variables: {
      CLIENT: 'greatwall-production',
      PLATFORM: 'copilot',
      STAGE: 'prod',
      PREVIOUS_STAGE: 'uat',
    },
    environment: {
      name: 'prod',
    },
    needs: [
      {
        pipeline: '$PARENT_PIPELINE_ID',
        job: 'build',
      },
      'postman-uat', 
    ],
  },
  'postman-prod': {
    extends: [
      '.postman',
    ],
    stage: 'test-prod',
    variables: {
      POSTMAN_ENVIRONMENT_UUID: '$POSTMAN_PROD_ENVIRONMENT_UUID',
    },
    needs: [
      'deploy-prod',
    ],
  },
};

boilerplate + std.manifestYamlDoc(pipeline, indent_array_in_object=true)