#!/usr/bin/env sh

set -eu

OUT_DIR=$1

mkdir -p $OUT_DIR
for file in `find src -name "*.jsonnet"`; do
  filename=$(basename -- $file)
  filename=${filename%.*}
  jsonnet -S $file > $OUT_DIR/${filename}.yml
done